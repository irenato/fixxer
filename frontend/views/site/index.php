<?php

/* @var $this yii\web\View */

$this->title = 'Fixxer';
?>
<main>
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="baner-wrapper">
                    <div class="banner-inner">
                        <h1>ПЕРВЫЙ СЕРВИС ДЛЯ СОТРУДНИЧЕСТВА СТО И АВТОВЛАДЕЛЬЦЕВ</h1>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Поиск категории">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> По категории
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">По категории</a></li>
                                    <li><a href="#">По мастерам</a></li>
                                </ul>
                            </div>
                            <span class="input-group-btn">
									<button class="btn btn-secondary" type="button">
									  <i class="fa fa-search" aria-hidden="true"></i>
									</button>
								  </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="masters">
        <div class="container">
            <div class="row">


                <h2>ЛУЧШИЕ МАСТЕРА <span class="in-category"></span><span class="in-city"></span></h2>
                <div class="parameters">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-10">
                        <div class="btn-group city">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Все регионы
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Eng</a></li>
                                <li><a href="#">Укр</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-4 col-xs-10">
                        <div class="btn-group category">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Все категории
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Общая диагностика</a></li>
                                <li><a href="#">Электроника</a></li>
                                <li><a href="#">Рихтовка</a></li>
                                <li><a href="#">Сварка</a></li>
                                <li><a href="#">Покраска</a></li>
                                <li><a href="#">Шиномантаж</a></li>
                                <li><a href="#">Химчистка</a></li>
                                <li><a href="#">Реставрация салона</a></li>
                                <li><a href="#">Тюнинг</a></li>
                                <li><a href="#">Медвежатники</a></li>
                                <li><a href="#">Мастер на выезд</a></li>
                                <li><a href="#">Аренда инструментов</a></li>
                                <li><a href="#">Эвакуаторы</a></li>
                                <li><a href="#">Предпродажная подготовка</a></li>
                                <li><a href="#">Другие услуги</a></li>
                                <li><a href="#">Ремонт двигателей</a></li>
                                <li><a href="#">Ремонт ходовой</a></li>
                                <li><a href="#">Аренда помещения под ремонт</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="masters-slider">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="master">
                            <img src="images/photo.png" alt="photo">
                            <div class="name-sto">СТО «100»</div>
                            <ul class="stars">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <div class="evaluation">
                                (<span>150</span> оценок)
                            </div>
                            <div class="location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span class="city-location">Харьков</span>
                            </div>
                            <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="master">
                            <img src="images/photo.png" alt="photo">
                            <div class="name-sto">СТО «100»</div>
                            <ul class="stars">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <div class="evaluation">
                                (<span>150</span> оценок)
                            </div>
                            <div class="location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span class="city-location">Харьков</span>
                            </div>
                            <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="master">
                            <img src="images/photo.png" alt="photo">
                            <div class="name-sto">СТО «100»</div>
                            <ul class="stars">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <div class="evaluation">
                                (<span>150</span> оценок)
                            </div>
                            <div class="location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span class="city-location">Харьков</span>
                            </div>
                            <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="master">
                            <img src="images/photo.png" alt="photo">
                            <div class="name-sto">СТО «100»</div>
                            <ul class="stars">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <div class="evaluation">
                                (<span>150</span> оценок)
                            </div>
                            <div class="location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span class="city-location">Харьков</span>
                            </div>
                            <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="master">
                            <img src="images/photo.png" alt="photo">
                            <div class="name-sto">СТО «100»</div>
                            <ul class="stars">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <div class="evaluation">
                                (<span>150</span> оценок)
                            </div>
                            <div class="location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span class="city-location">Харьков</span>
                            </div>
                            <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="master">
                            <img src="images/photo.png" alt="photo">
                            <div class="name-sto">СТО «100»</div>
                            <ul class="stars">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <div class="evaluation">
                                (<span>150</span> оценок)
                            </div>
                            <div class="location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span class="city-location">Харьков</span>
                            </div>
                            <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="master">
                            <img src="images/photo.png" alt="photo">
                            <div class="name-sto">СТО «100»</div>
                            <ul class="stars">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <div class="evaluation">
                                (<span>150</span> оценок)
                            </div>
                            <div class="location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span class="city-location">Харьков</span>
                            </div>
                            <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="master">
                            <img src="images/photo.png" alt="photo">
                            <div class="name-sto">СТО «100»</div>
                            <ul class="stars">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <div class="evaluation">
                                (<span>150</span> оценок)
                            </div>
                            <div class="location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span class="city-location">Харьков</span>
                            </div>
                            <div class="button green"><a href=""><span>НАНЯТЬ</span></a></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="advantages">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-3 col-xs-6">
                    <div class="advantage">
                        <img src="images/advant.png" alt="photo">
                        <h3>ПРЕИМУЩЕСТВО</h3>
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-xs-6">
                    <div class="advantage">
                        <img src="images/advant.png" alt="photo">
                        <h3>ПРЕИМУЩЕСТВО</h3>
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-xs-6">
                    <div class="advantage">
                        <img src="images/advant.png" alt="photo">
                        <h3>ПРЕИМУЩЕСТВО</h3>
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-xs-6">
                    <div class="advantage">
                        <img src="images/advant.png" alt="photo">
                        <h3>ПРЕИМУЩЕСТВО</h3>
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="last-orders">
        <div class="container">
            <div class="row">
                <h2>ПОСЛЕДНИЕ ЗАКАЗЫ</h2>
                <div class="parameters">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-10">
                        <div class="btn-group city">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Все регионы
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Eng</a></li>
                                <li><a href="#">Укр</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-4 col-xs-10">
                        <div class="btn-group category">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Все категории
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Общая диагностика</a></li>
                                <li><a href="#">Электроника</a></li>
                                <li><a href="#">Рихтовка</a></li>
                                <li><a href="#">Сварка</a></li>
                                <li><a href="#">Покраска</a></li>
                                <li><a href="#">Шиномантаж</a></li>
                                <li><a href="#">Химчистка</a></li>
                                <li><a href="#">Реставрация салона</a></li>
                                <li><a href="#">Тюнинг</a></li>
                                <li><a href="#">Медвежатники</a></li>
                                <li><a href="#">Мастер на выезд</a></li>
                                <li><a href="#">Аренда инструментов</a></li>
                                <li><a href="#">Эвакуаторы</a></li>
                                <li><a href="#">Предпродажная подготовка</a></li>
                                <li><a href="#">Другие услуги</a></li>
                                <li><a href="#">Ремонт двигателей</a></li>
                                <li><a href="#">Ремонт ходовой</a></li>
                                <li><a href="#">Аренда помещения под ремонт</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="orders-list">
                    <div class="order">
                        <div class="order-slatus urgent">
                            <img src="images/fire.png" alt="fire">
                            <span>Срочный</span>
                        </div>
                        <div class="left-side">
                            <h4>Ремонт двигателя автомобиля</h4>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                <a href="#">Подробнее</a>
                            </p>
                            <ul class="order-info">
                                <li class="relevant">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>Актуален&shy; <span class="days"> 6 дней &shy;</span> <span class="hour">18 часов</span>
                                </li>
                                <li class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>Харьков
                                </li>
                            </ul>
                        </div>
                        <div class="right-side">
                            <div class="cost">
                                <span class="budget">Бюджет: </span>
                                <span class="sum">5000 грн.</span>
                            </div>
                            <div class="button green">
                                <a href="#">
                                    <span>ВЫПОЛНИТЬ</span>
                                </a>
                            </div>
                            <div class="user">
                                <span class="user-name">Сидор Вишевский</span>
                                <img src="images/user-photo.png" alt="user-photo" class="user-hpoto">
                            </div>
                        </div>
                    </div>
                    <div class="order">
                        <div class="order-slatus">
                            <img src="images/fire.png" alt="fire">
                            <span>Срочный</span>
                        </div>
                        <div class="left-side">
                            <h4>Ремонт двигателя автомобиля</h4>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                <a href="#">Подробнее</a>
                            </p>
                            <ul class="order-info">
                                <li class="relevant">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>Актуален&shy; <span class="days"> 6 дней &shy;</span> <span class="hour">18 часов</span>
                                </li>
                                <li class="location">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>Харьков
                                </li>
                            </ul>
                        </div>
                        <div class="right-side">
                            <div class="cost">
                                <span class="budget">Бюджет: </span>
                                <span class="sum">5000 грн.</span>
                            </div>
                            <div class="button green">
                                <a href="#">
                                    <span>ВЫПОЛНИТЬ</span>
                                </a>
                            </div>
                            <div class="user">
                                <span class="user-name">Сидор Вишевский</span>
                                <img src="images/user-photo.png" alt="user-photo" class="user-hpoto">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button transparent grey"><a href="#"><span>ПОКАЗАТЬ БОЛЬШЕ</span></a></div>
            </div>
        </div>
    </div>
</main>
<!--END CONTENT-->

<!--MODAL ENTER-->
<div class="modal-win enter">
    <div class="container">
        <div class="row">

            <h2>ВХОД НА САЙТ</h2>
            <div class="close-modal">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <form action="#">
                <h3>Вход через соц.сети</h3>
                <div class="social">
                    <div class="social-inner">
                        <div class="facebook">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </div>
                        <div class="vk">
                            <i class="fa fa-vk" aria-hidden="true"></i>
                        </div>
                        <div class="ok">
                            <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="line-bt"></div>
                <div class="input-field">
                    <input type="email" placeholder="E-mail" required>
                </div>
                <div class="input-field">
                    <input type="password" placeholder="Пароль" required>
                </div>
                <div class="forgot-pasword">
                    <a href="#">Забыли пароль?</a>
                </div>
                <div class="button green">
                    <input type="submit" value="ВОЙТИ">
                </div>
                <div class="line-bt"></div>
                <span>Ещё не зарегестрированы? <a href="#">Вперёд!</a></span>
            </form>
        </div>
    </div>
</div>

<!--MODAL CLIENT REGISTRATION-->
<div class="modal-win clientReg">
    <div class="container">
        <div class="row">

            <h2>Регистрация клиента</h2>
            <h3>Зарегестрируйтесь как клиент, чтобы иметь возможность записываться на ремонт в лучшие СТО и выкладывать объявления о ремонте авто</h3>
            <div class="close-modal">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <form action="#">
                <div class="form-header">
                    <div class="step active">
                        <div class="general-info"><span>1</span></div>
                        <p>Общая информация</p>
                    </div>
                    <div class="step step2">
                        <div class="client-info"><span>2</span></div>
                        <p>Информация о клиенте</p>
                    </div>
                    <span class="progress"></span>
                </div>
                <div class="form-content">
                    <div id="content1" class="general-info content">
                        <div class="input-field">
                            <input type="email" name="email" placeholder="E-mail*" required>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password" placeholder="Пароль*" required>
                        </div>
                        <div class="input-field">
                            <input type="tel" name="tel" placeholder="Телефон*" required>
                        </div>
                        <div class="input-field">
                            <input type="password" name="iteratePassword" placeholder="Подтвердить пароль*" required>
                        </div>
                        <div class="button green"><a href="#content2" class="toggle"><span>ДАЛЕЕ</span></a></div>
                    </div>


                    <!-- ----------------------- content2 --------------------------->
                    <div id="content2" class="client-info content">
                        <div class="photo">
                            <input type="file" id="file" name="file" style="display:none;" />
                            <div class="client-photo">
                                <span id="output"></span>
                            </div>
                            <h4>Ваша фотография <br>(необязательно)</h4>
                        </div>
                        <div class="input-field">
                            <input type="text" name="firstName" placeholder="Имя*" required>
                        </div>
                        <div class="input-field">
                            <input type="text" name="lastName" placeholder="Фамилия*" required>
                        </div>
                        <div class="btn-group area">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> Ваша область*
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">some text</a></li>
                                <li><a href="#">some text</a></li>
                            </ul>
                        </div>

                        <div class="button grey"><a href="#content1" class="toggle"><span>
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
								НАЗАД
							</span></a></div>
                        <div class="button green">
                            <input type="submit" value="ЗАРЕГЕСТРИРОВАТЬСЯ">
                        </div>
                    </div>
                </div>
                <div class="form-footer">
                    <span class="title-social">Регистрация через соц. сети &nbsp;</span>
                    <div class="social">
                        <div class="social-inner">
                            <div class="facebook">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </div>
                            <div class="vk">
                                <i class="fa fa-vk" aria-hidden="true"></i>
                            </div>
                            <div class="ok">
                                <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <br>
                    <span>Уже зарегестрированы?<a href="#"> Войти! </a></span>
                </div>


            </form>
        </div>
    </div>
</div>
<!--MODAL CLIENT REGISTRATION-->
<div class="modal-win masterReg">
    <div class="container">
        <div class="row">

            <h2>Регистрация СТО/Мастера</h2>
            <h3>Зарегестрируйтесь как СТО/Мастер и берите заказы от автовладельцев и осуществляйте эффективный трекинг заказов через наш сайт</h3>
            <div class="close-modal">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <form action="#">
                <div class="form-header">
                    <div class="step active">
                        <div class="general-info"><span>1</span></div>
                        <p>Общая информация</p>
                    </div>
                    <div class="step step2">
                        <div class="client-info"><span>2</span></div>
                        <p class="master">Информация о Мастере</p>
                        <p class="sto" hidden>Информация об СТО</p>
                    </div>
                    <span class="progress"></span>
                    <div class="step step3">
                        <div class="client-info"><span>3</span></div>
                        <p>Навыки</p>
                    </div>
                    <span class="progress progress2"></span>
                </div>
                <div class="form-content">
                    <div id="masterContent1" class="general-info content">
                        <div class="input-group inline">
                            <h3>Выберите ваш вид деятельности:</h3>

                            <input type="radio" name="optradio" id="master" hidden checked>
                            <label class="radio-inline" for="master">Мастер</label>

                            <input type="radio" name="optradio" id="sto" hidden>
                            <label class="radio-inline" for="sto">СТО</label>
                        </div>
                        <div class="input-field">
                            <input type="email" name="email" placeholder="E-mail*" required>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password" placeholder="Пароль*" required>
                        </div>
                        <div class="input-field tel">
                            <input type="tel" name="tel" placeholder="Телефон*" required>
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </div>
                        <div class="input-field">
                            <input type="password" name="iteratePassword" placeholder="Подтвердить пароль*" required>
                        </div>
                        <div class="clearfix"></div>
                        <div class="addedTel"></div>
                        <p class="addTelNumber">Добавить доп. номер телефона <i class="fa fa-plus" aria-hidden="true"></i></p>
                        <div class="button green"><a href="#masterContent2"  class="toggle"><span>ДАЛЕЕ</span></a></div>
                    </div>


                    <!-- ----------------------- content2 --------------------------->
                    <div id="masterContent2" class="client-info content">
                        <div class="sto masterContent2Optional">
                            <div class="photo">
                                <input type="file" id="file" name="file" style="display:none;" />
                                <div class="sto-photo">
                                    <span id="output"></span>
                                </div>
                                <h4>Логотип СТО <br>(необязательно)</h4>
                            </div>
                            <div class="input-field">
                                <input type="text" name="firstName" placeholder="Название СТО*" required>
                            </div>
                            <div class="input-field">
                                <input type="text" name="area" placeholder="Область*" required>
                            </div>
                            <div class="input-field">
                                <input type="text" name="firstName" placeholder="Имя*" required>
                            </div>
                            <div class="input-field">
                                <input type="text" name="city" placeholder="Город*" required>
                            </div>
                            <div class="input-field">
                                <input type="text" name="lastName" placeholder="Фамилия*" required>
                            </div>
                            <div class="input-field addres">
                                <input type="text" name="street" placeholder="Улица*" required>
                                <input type="text" name="house" placeholder="Дом*" required>
                            </div>
                            <div class="input-field schedule">
                                <span>Расписание*:</span>
                                <input type="text" name="days" class="days" placeholder="Дни*" required>
                                <input type="text" name="hours" class="hours" placeholder="Время*" required>

                                <div class="drop-down days">
                                    <div class="btn-group start">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">Пн</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Пн</a></li>
                                            <li><a href="#">Вт</a></li>
                                            <li><a href="#">Ср</a></li>
                                            <li><a href="#">Чт</a></li>
                                            <li><a href="#">Пт</a></li>
                                            <li><a href="#">Сб</a></li>
                                            <li><a href="#">Вс</a></li>
                                        </ul>
                                    </div>
                                    <div class="tire">-</div>
                                    <div class="btn-group end">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                            <span class="name">Пт</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Пн</a></li>
                                            <li><a href="#">Вт</a></li>
                                            <li><a href="#">Ср</a></li>
                                            <li><a href="#">Чт</a></li>
                                            <li><a href="#">Пт</a></li>
                                            <li><a href="#">Сб</a></li>
                                            <li><a href="#">Вс</a></li>
                                        </ul>
                                    </div>
                                    <div class="button green"><a href="#"><span>OK</span></a></div>
                                    <div class="round-the-clock">
                                    </div>
                                </div>

                                <div class="drop-down hours">
                                    <div class="btn-group start">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">9:00</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">1:00</a></li>
                                            <li><a href="#">2:00</a></li>
                                            <li><a href="#">3:00</a></li>
                                            <li><a href="#">4:00</a></li>
                                            <li><a href="#">5:00</a></li>
                                            <li><a href="#">6:00</a></li>
                                            <li><a href="#">7:00</a></li>
                                            <li><a href="#">8:00</a></li>
                                            <li><a href="#">9:00</a></li>
                                            <li><a href="#">10:00</a></li>
                                            <li><a href="#">11:00</a></li>
                                            <li><a href="#">12:00</a></li>
                                            <li><a href="#">13:00</a></li>
                                            <li><a href="#">14:00</a></li>
                                            <li><a href="#">15:00</a></li>
                                            <li><a href="#">16:00</a></li>
                                            <li><a href="#">17:00</a></li>
                                            <li><a href="#">18:00</a></li>
                                            <li><a href="#">19:00</a></li>
                                            <li><a href="#">20:00</a></li>
                                            <li><a href="#">21:00</a></li>
                                            <li><a href="#">22:00</a></li>
                                            <li><a href="#">23:00</a></li>
                                            <li><a href="#">24:00</a></li>
                                        </ul>
                                    </div>
                                    <div class="tire">-</div>
                                    <div class="btn-group end">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">18:00</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">1:00</a></li>
                                            <li><a href="#">2:00</a></li>
                                            <li><a href="#">3:00</a></li>
                                            <li><a href="#">4:00</a></li>
                                            <li><a href="#">5:00</a></li>
                                            <li><a href="#">6:00</a></li>
                                            <li><a href="#">7:00</a></li>
                                            <li><a href="#">8:00</a></li>
                                            <li><a href="#">9:00</a></li>
                                            <li><a href="#">10:00</a></li>
                                            <li><a href="#">11:00</a></li>
                                            <li><a href="#">12:00</a></li>
                                            <li><a href="#">13:00</a></li>
                                            <li><a href="#">14:00</a></li>
                                            <li><a href="#">15:00</a></li>
                                            <li><a href="#">16:00</a></li>
                                            <li><a href="#">17:00</a></li>
                                            <li><a href="#">18:00</a></li>
                                            <li><a href="#">19:00</a></li>
                                            <li><a href="#">20:00</a></li>
                                            <li><a href="#">21:00</a></li>
                                            <li><a href="#">22:00</a></li>
                                            <li><a href="#">23:00</a></li>
                                            <li><a href="#">24:00</a></li>
                                        </ul>
                                    </div>
                                    <div class="button green"><a href="#"><span>OK</span></a></div>
                                    <div class="round-the-clock">
                                        <input type="checkbox" class="roundClock" id="roundClockSto" hidden="">
                                        <label for="roundClockSto">Круглосуточно</label>
                                    </div>
                                </div>

                            </div>

                            <div class="input-field">
                            </div>

                            <div class="clearfix"></div>
                            <div class="addedAdres">
                                <div class="addedAdres-inner">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                    <div class="input-field">
                                        <input type="text" name="area" placeholder="Область*" required>
                                    </div>
                                    <div class="input-field">
                                        <input type="text" name="city" placeholder="Город*" required>
                                    </div>
                                    <div class="input-field addres">
                                        <input type="text" name="street" placeholder="Улица*" required>
                                        <input type="text" name="house" placeholder="Дом*" required>
                                    </div>
                                </div>
                            </div>

                            <p class="addAddres">Добавить доп. адрес <i class="fa fa-plus" aria-hidden="true"></i></p>
                            <textarea name="description" placeholder="Описание"></textarea>
                        </div>
                        <div class="master masterContent2Optional">
                            <div class="photo">
                                <input type="file" id="file" name="file" style="display:none;" />
                                <div class="client-photo">
                                    <span id="output"></span>
                                </div>
                                <h4>Ваша фотография <br>(необязательно)</h4>
                            </div>
                            <div class="input-field">
                                <input type="text" name="firstName" placeholder="Имя*" required>
                            </div>

                            <div class="input-field">
                                <input type="text" name="area" placeholder="Область*" required>
                            </div>
                            <div class="input-field">
                                <input type="text" name="lastName" placeholder="Фамилия*" required>
                            </div>
                            <div class="input-field">
                                <input type="text" name="city" placeholder="Город*" required>
                            </div>
                            <div class="input-field schedule">
                                <span>Расписание*:</span>
                                <input type="text" name="days" class="days" placeholder="Дни*" required>
                                <input type="text" name="hours" class="hours" placeholder="Время*" required>

                                <div class="drop-down days">
                                    <div class="btn-group start">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">Пн</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Пн</a></li>
                                            <li><a href="#">Вт</a></li>
                                            <li><a href="#">Ср</a></li>
                                            <li><a href="#">Чт</a></li>
                                            <li><a href="#">Пт</a></li>
                                            <li><a href="#">Сб</a></li>
                                            <li><a href="#">Вс</a></li>
                                        </ul>
                                    </div>
                                    <div class="tire">-</div>
                                    <div class="btn-group end">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                            <span class="name">Пт</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Пн</a></li>
                                            <li><a href="#">Вт</a></li>
                                            <li><a href="#">Ср</a></li>
                                            <li><a href="#">Чт</a></li>
                                            <li><a href="#">Пт</a></li>
                                            <li><a href="#">Сб</a></li>
                                            <li><a href="#">Вс</a></li>
                                        </ul>
                                    </div>
                                    <div class="button green"><a href="#"><span>OK</span></a></div>
                                    <div class="round-the-clock">
                                    </div>
                                </div>

                                <div class="drop-down hours">
                                    <div class="btn-group start">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">9:00</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">1:00</a></li>
                                            <li><a href="#">2:00</a></li>
                                            <li><a href="#">3:00</a></li>
                                            <li><a href="#">4:00</a></li>
                                            <li><a href="#">5:00</a></li>
                                            <li><a href="#">6:00</a></li>
                                            <li><a href="#">7:00</a></li>
                                            <li><a href="#">8:00</a></li>
                                            <li><a href="#">9:00</a></li>
                                            <li><a href="#">10:00</a></li>
                                            <li><a href="#">11:00</a></li>
                                            <li><a href="#">12:00</a></li>
                                            <li><a href="#">13:00</a></li>
                                            <li><a href="#">14:00</a></li>
                                            <li><a href="#">15:00</a></li>
                                            <li><a href="#">16:00</a></li>
                                            <li><a href="#">17:00</a></li>
                                            <li><a href="#">18:00</a></li>
                                            <li><a href="#">19:00</a></li>
                                            <li><a href="#">20:00</a></li>
                                            <li><a href="#">21:00</a></li>
                                            <li><a href="#">22:00</a></li>
                                            <li><a href="#">23:00</a></li>
                                            <li><a href="#">24:00</a></li>
                                        </ul>
                                    </div>
                                    <div class="tire">-</div>
                                    <div class="btn-group end">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span> <span class="name">18:00</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">1:00</a></li>
                                            <li><a href="#">2:00</a></li>
                                            <li><a href="#">3:00</a></li>
                                            <li><a href="#">4:00</a></li>
                                            <li><a href="#">5:00</a></li>
                                            <li><a href="#">6:00</a></li>
                                            <li><a href="#">7:00</a></li>
                                            <li><a href="#">8:00</a></li>
                                            <li><a href="#">9:00</a></li>
                                            <li><a href="#">10:00</a></li>
                                            <li><a href="#">11:00</a></li>
                                            <li><a href="#">12:00</a></li>
                                            <li><a href="#">13:00</a></li>
                                            <li><a href="#">14:00</a></li>
                                            <li><a href="#">15:00</a></li>
                                            <li><a href="#">16:00</a></li>
                                            <li><a href="#">17:00</a></li>
                                            <li><a href="#">18:00</a></li>
                                            <li><a href="#">19:00</a></li>
                                            <li><a href="#">20:00</a></li>
                                            <li><a href="#">21:00</a></li>
                                            <li><a href="#">22:00</a></li>
                                            <li><a href="#">23:00</a></li>
                                            <li><a href="#">24:00</a></li>
                                        </ul>
                                    </div>
                                    <div class="button green"><a href="#"><span>OK</span></a></div>
                                    <div class="round-the-clock">
                                        <input type="checkbox" class="roundClock" id="roundClockMaster" hidden="">
                                        <label for="roundClockMaster">Круглосуточно</label>
                                    </div>
                                </div>

                            </div>
                            <div class="input-field addres">
                                <input type="text" name="street" placeholder="Улица*" required>
                                <input type="text" name="house" placeholder="Дом*" required>
                            </div>
                            <div class="clearfix"></div>
                            <div class="addedAdres">
                                <div class="addedAdres-inner">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                    <div class="input-field">
                                        <input type="text" name="area" placeholder="Область*" required>
                                    </div>
                                    <div class="input-field">
                                        <input type="text" name="city" placeholder="Город*" required>
                                    </div>
                                    <div class="input-field addres">
                                        <input type="text" name="street" placeholder="Улица*" required>
                                        <input type="text" name="house" placeholder="Дом*" required>
                                    </div>
                                </div>
                            </div>
                            <p class="addAddres">Добавить доп. адрес <i class="fa fa-plus" aria-hidden="true"></i></p>
                            <textarea name="description" placeholder="Описание"></textarea>
                        </div>
                        <div class="button grey"><a href="#masterContent1" class="toggle"><span>
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
								НАЗАД
							</span></a></div>
                        <div class="button green"><a href="#masterContent3" class="toggle"><span>
								ДАЛЕЕ
							</span></a></div>
                    </div>

                    <!-- ----------------------- content3 --------------------------->
                    <div id="masterContent3" class="client-info content">

                        <div class="skills">
                            <div class="skills-header">
                                <h3>Выберите ваши навыки</h3>
                                <div class="counter">Выбрано <span class="chosen">0</span>/<span class="allCounter">15</span> навыков</div>
                            </div>
                            <div class="skills-body">
                                <ul>
                                    <li>Общая диагностика<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Электроника<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Рихтовка<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Сварка<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Покраска<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Шиномантаж<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Химчистка<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Реставрация салона<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Тюнинг<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Медвежатники<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Мастер на выезд<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Аренда инструментов<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Эвакуаторы<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Другие услуги<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Предпродажная подготовка<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Ремонт двигателей<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Ремонт ходовой<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                    <li>Аренда помещения под ремонт<i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></li>
                                </ul>
                            </div>
                        </div>

                        <div class="button grey"><a href="#masterContent2" class="toggle"><span>
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
								НАЗАД
							</span></a></div>
                        <div class="button green">
                            <input type="submit" value="ЗАРЕГЕСТРИРОВАТЬСЯ">
                        </div>
                    </div>
                </div>
                <div class="form-footer">
                    <span class="title-social">Регистрация через соц. сети &nbsp;</span>
                    <div class="social">
                        <div class="social-inner">
                            <div class="facebook">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </div>
                            <div class="vk">
                                <i class="fa fa-vk" aria-hidden="true"></i>
                            </div>
                            <div class="ok">
                                <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <br>
                    <span>Уже зарегестрированы?<a href="#"> Войти! </a></span>
                </div>


            </form>
        </div>
    </div>
</div>
