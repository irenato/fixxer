<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AutoFix</title>
    <?php $this->head() ?>
</head>

<body>
<!--START HEADER-->
<header class="landing-page">
    <div class="container">
        <div class="row">
            <div class="wrapper">
                <a class="logo" href="/">
                    <img src="/images/LOGO.png" alt="logo">
                </a>
                <div class="right-side">
                    <div class="lang">

                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                Рус
                                <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Eng</a></li>
                                <li><a href="#">Укр</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="enter open-modal" data-modal="enter">
                        <a href="#">ВХОД</a>
                    </div>
                    <div class="button green open-modal" data-modal="clientReg">
                        <a href="#"><span>НУЖЕН РЕМОНТ</span></a>
                    </div>
                    <div class="button transparent  open-modal" data-modal="masterReg">
                        <a href="#"><span>СТАТЬ МАСТЕРОМ</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
<?php $this->beginBody() ?>

        <?= $content ?>

<footer class="landing-page">

    <div class="footer-top">
        <div class="container">
            <div class="row">
                <ul>
                    <span>КАТЕГОРИЯ МЕНЮ</span>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                </ul>
                <ul>
                    <span>КАТЕГОРИЯ МЕНЮ</span>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                </ul>
                <ul>
                    <span>КАТЕГОРИЯ МЕНЮ</span>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                </ul>
                <ul>
                    <span>КАТЕГОРИЯ МЕНЮ</span>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                    <li>Пункт меню</li>
                </ul>
                <ul>
                    <span>КОНТАКТЫ</span>
                    <li>Номер телефона и тд и тп</li>
                    <li>Электронная почта</li>
                    <li>Адрес, страна и тд и тп</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="left-side">
                    <a class="logo" href="/">
                        <img src="/images/LOGO-blue.png" alt="LOGO-blue">
                    </a>
                    <div class="social">
                        <div class="facebook">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </div>
                        <div class="vk">
                            <i class="fa fa-vk" aria-hidden="true"></i>
                        </div>
                        <div class="ok">
                            <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <span>Все права защищены 2016</span>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--END FOOTER-->

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
