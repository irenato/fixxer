$(document).ready(function () {
	var openModal;
	var focusSchedule = true;
	var validName = function (_this, val) {
		var rv_name = /^[а-яА-Я]+$/,
			that = _this;

		if (val.length >= 2 && val != '' && rv_name.test(val)) {
			$(that).removeClass('has-error').addClass('has-success');
		} else {
			$(that).removeClass('has-success').addClass('has-error');
		}
	},
	validEmail = function (_this, val, confirm) {
		var rv_mail = /.+@.+\..+/i,
				that = _this;

			if (val.length > 2 && val != '' && rv_mail.test(val)) {
				$(that).removeClass('has-error').addClass('has-success');
			} else {
				$(that).removeClass('has-success').addClass('has-error');
			}
		
	},
	validPassword = function (_this, val, confirm) {
		var rv_mail = /.{6,}/i,
			that = _this;
		
		if (confirm) {
			var passwordInput = $(that).closest('.content').find("[type='password']").val(),
				that = _this;

			if ($(that).val() === passwordInput && val !== '') {
				$(that).removeClass('has-error').addClass('has-success');
			} else {
				$(that).removeClass('has-success').addClass('has-error');
			}

		} else {

			if (val.length > 2 && val != '' && rv_mail.test(val)) {
				$(that).removeClass('has-error').addClass('has-success');
			} else {
				$(that).removeClass('has-success').addClass('has-error');
			}
		}
	},
	validDays = function (_this, val) {
		var rv_days = /^[а-яА-Я]+\s-\s[а-яА-Я]+$/,
			that = _this;

		if (val.length >= 2 && val != '' && rv_days.test(val)) {
			$(that).removeClass('has-error').addClass('has-success');
		} else {
			$(that).removeClass('has-success').addClass('has-error');
		}
	},
	validHours = function (_this, val) {
		var rv_days = /^[0-9]{1,2}:[0-9]{2}\s-\s[0-9]{2}:[0-9]{2}$/,
			that = _this;

		if (val.length >= 2 && val != '' && rv_days.test(val)) {
			$(that).removeClass('has-error').addClass('has-success');
		} else {
			$(that).removeClass('has-success').addClass('has-error');
		}
	}
	// --------------------------------------------------------------
	// Изменения хеадера при скролле (один раз)
	// --------------------------------------------------------------
	$(window).on('scroll', function () {
		if ($(this).scrollTop() > 0) {
			$('header.landing-page').addClass('fixed-scroll');
		} else if (!openModal) {
			$('header.landing-page').removeClass('fixed-scroll');
		}
	});
	// --------------------------------------------------------------
	// Модалка входа на сайт на landing-page
	// --------------------------------------------------------------
	$(".landing-page  .open-modal").click(function (e) {
		e.preventDefault();
		var modalWinName = $(this).attr('data-modal'),
			modalWin = $(".modal-win." + modalWinName);

		$(".modal-win").hide();
		$('header.landing-page').addClass('fixed-scroll');
		modalWin.fadeIn(500).find('.close-modal').click(function () {
			modalWin.hide();
			$('header.landing-page').removeClass('fixed-scroll');
			openModal = false;
		});
		openModal = true;

	});
	// --------------------------------------------------------------
	// Кнопки "далее" и "назад" в модалке регистрации на landing-page
	// --------------------------------------------------------------
	
	$(".form-content .button a.toggle").click(function (e) {
		e.preventDefault();
		var id = $(this).attr('href'),
			currentContent = $(this).closest(".content"),
			currentContentInputs = currentContent.find('input'),
			parentForm = currentContent.closest('.form-content'),
			formContent = parentForm.find('.content'),
			checkedInput = currentContent.find("input[name='optradio']:checked"),
			activity = '.' + checkedInput.attr('id');
		
		function transition() {
			formContent.hide();
			$(".form-content " + id).show();
		}
		
		
		
		if (id == '#content2' || id == '#masterContent2') {
			if ( currentContent.find('.has-success').length >= 3 ) {            // убрать равно когда подключишь валидацию  телефона
				$('.form-header .step2').addClass('active');
				transition();
				if ( checkedInput.length) {
					$(".form-content " + id).find('.masterContent2Optional').hide();
					$(".form-content " + id).find(activity).show();
					parentForm.prev('.form-header').find('.step2 p').hide();
					parentForm.prev('.form-header').find('.step2 ' + activity).show();
				}
			} else {
				if ( currentContentInputs.length > 1 ) {
					currentContentInputs.blur();
				} else {
					transition();
				}
			}
		} else if ( id == '#content1' || id == '#masterContent1' ){
			$('.form-header .step2, .form-header .step3').removeClass('active');
			transition();
		} else if (id == '#masterContent3') {
			if ( currentContent.find('.has-success').length >= 5 ) {            // убрать равно когда подключишь валидацию  телефона
				$('.form-header .step3').addClass('active');
				transition();
			} else {
				if ( currentContentInputs.length > 1 ) {
					currentContentInputs.blur();
				}
			}
		} else {
			transition();
		}
	
			
	});

	// --------------------------------------------------------------
	// Выбор расписания работы в модалке регистрации
	// --------------------------------------------------------------
	var schedule = $('.schedule'),
		scheduleDropDownWin = schedule.find('.drop-down'),
		name;
		
	$(".schedule>input").on({
		click: function() {
				name = "." + $(this).attr('name');
				scheduleDropDownWin.hide();
				schedule.find(name).show();
		}
	});
	scheduleDropDownWin.find('.button').click(function() {
		var str = '',
			roundTheClock = $(this).closest('.drop-down').find('.roundClock:checked').length;
		if (roundTheClock) {
			schedule.find("input" + name).val('0:00 - 24:00').blur();
		} else {
			$(this).closest('.drop-down').find('.name').each(function(index, el) {
				index ? str += el.innerHTML : str += el.innerHTML + ' - ';
				console.log(str);
			});
			schedule.find("input" + name).val(str).blur();
		}
		$(this).closest('.drop-down').hide();
	});
	scheduleDropDownWin.find('.dropdown-menu a').click(function(e) {
		e.preventDefault();
		var text = $(this).html();
		$(this).closest('.btn-group').find('button .name').html(text);
	});
	scheduleDropDownWin.find('.dropdown-menu').niceScroll({
      cursoropacitymax: 0
    });
	
	// --------------------------------------------------------------
	// Добавить адрес
	// --------------------------------------------------------------
	var addresField = $('.addedAdres-inner');
	$("p.addAddres").click(function() {
		addresField.css('display') == 'none' ? addresField.css('display', 'block') : $('.addedAdres').append(addresField.clone());
	});
	$('body').on('click', '.addedAdres-inner i', function() {
		$(this).closest('.addedAdres-inner').remove();
	});
	// --------------------------------------------------------------
	// Добавить номер телефона
	// --------------------------------------------------------------
	$('.addTelNumber').click(function() {
		var telField = $(this).closest('#masterContent1').find("input[name='tel']").closest('.input-field');
		$('.addedTel').append($(telField[0]).clone());
	});
	$('body').on('click', '.input-field.tel i', function() {
		$(this).closest('.input-field').remove();
	});
	// --------------------------------------------------------------
	// Добавить фото при регистрации
	// --------------------------------------------------------------
	$(".client-photo").on("click", function () {
		$('#file').trigger("click");
	});

	function handleFileSelect(evt) {
		var file = evt.target.files; // FileList object
		var f = file[0];
		// Only process image files.
		if (!f.type.match('image.*')) {
			alert("Image only please....");
		}
		var reader = new FileReader();
		// Closure to capture the file information.
		reader.onload = (function (theFile) {
			return function (e) {
				// Render thumbnail.
				var span = document.createElement('span');
				span.innerHTML = ['<img class="thumb" title="', escape(theFile.name), '" src="', e.target.result, '" />'].join('');
				document.getElementById('output').insertBefore(span, null);
			};
		})(f);
		// Read in the image file as a data URL.
		reader.readAsDataURL(f);
	}
	document.getElementById('file').addEventListener('change', handleFileSelect, false);
	// --------------------------------------------------------------
	// Выбор навыков при регистрации мастара
	// --------------------------------------------------------------
	$("#masterContent3 .allCounter").html($("#masterContent3 .skills-body li").length);
	$("#masterContent3 .skills-body li").click(function() {
		$(this).toggleClass('active');
		$("#masterContent3 .chosen").html($("#masterContent3 .skills-body .active").length);
	});
	// --------------------------------------------------------------
	// Валидация полей формы
	// --------------------------------------------------------------
	$('.form-content input').blur(function () {
		
		 var name = $(this).attr('name'),
			 val = $(this).val();

        switch (name) {
        case 'iteratePassword':
          validPassword(this, val, true);
          break;
        case 'password':
          validPassword(this, val);
          break;
        case 'email':
          validEmail(this, val);
          break;
        case 'tel':
          break;
        case 'firstName':
          validName(this, val);
          break;
        case 'lastName':
          validName(this, val);
          break;
		case 'days':
          validDays(this, val);
          break;
		case 'hours':
          validHours(this, val);
          break;
        } // end switch(...)
	});
	// --------------------------------------------------------------
	// Слайдер мастеров
	// --------------------------------------------------------------
	$('.masters-slider').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		autoplay: true,
		autoplaySpeed: 3000,
		prevArrow: '<div class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
		nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>',
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true
				}
    },
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
    },
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
    }
  ]
	});
});
