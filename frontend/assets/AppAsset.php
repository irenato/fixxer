<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/bootstrap-grid-3.3.1.min.css',
        'css/font-awesome.min.css',
        'css/fonts.css',
        'css/main.css',
        'css/slick.css',
    ];
    public $js = [
        'js/jquery.fancybox.js',
        'js/jquery.fancybox.pack.js',
        'js/jquery.nicescroll.min.js',
        'js/jquery.cloud9carousel.js',
        'js/mixitup.min.js',
        'js/owl.carousel.min.js',
        'js/slick.min.js',
        'js/bootstrap.min.js',
        'js/common.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
