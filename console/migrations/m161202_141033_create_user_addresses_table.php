<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_addresses`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m161202_141033_create_user_addresses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_addresses', [
            'id' => $this->primaryKey(),
            'place_id' => $this->string(45),
            'longitude' => $this->string(45),
            'latitude' => $this->string(45),
            'region' => $this->string(45),
            'user_id' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_addresses-user_id',
            'user_addresses',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_addresses-user_id',
            'user_addresses',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_addresses-user_id',
            'user_addresses'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_addresses-user_id',
            'user_addresses'
        );

        $this->dropTable('user_addresses');
    }
}
