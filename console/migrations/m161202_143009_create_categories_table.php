<?php

use yii\db\Migration;

/**
 * Handles the creation of table `categories`.
 */
class m161202_143009_create_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'title_en' => $this->string(45),
            'title_ru' => $this->string(45),
            'title_ua' => $this->string(45),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('categories');
    }
}
