<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auto_series`.
 */
class m161202_145041_create_auto_series_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('auto_series', [
            'id' => $this->primaryKey(),
            'model_id' => $this->string(45),
            'series_name' => $this->string(45),
            'generation_id' => $this->string(45),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auto_series');
    }
}
