<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_phones`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m161202_141504_create_user_phones_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_phones', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(45),
            'user_id' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_phones-user_id',
            'user_phones',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_phones-user_id',
            'user_phones',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_phones-user_id',
            'user_phones'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_phones-user_id',
            'user_phones'
        );

        $this->dropTable('user_phones');
    }
}
