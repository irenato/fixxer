<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auto_models`.
 */
class m161202_144209_create_auto_models_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('auto_models', [
            'id' => $this->primaryKey(),
            'brand_id' => $this->string(45),
            'model_name' => $this->string(45),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auto_models');
    }
}
