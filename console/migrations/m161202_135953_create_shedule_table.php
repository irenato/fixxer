<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shedule`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m161202_135953_create_shedule_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('shedule', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'from_days' => $this->integer(),
            'to_days' => $this->integer(),
            'from_hours' => $this->string(45),
            'to_hours' => $this->string(45),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-shedule-user_id',
            'shedule',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-shedule-user_id',
            'shedule',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-shedule-user_id',
            'shedule'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-shedule-user_id',
            'shedule'
        );

        $this->dropTable('shedule');
    }
}
