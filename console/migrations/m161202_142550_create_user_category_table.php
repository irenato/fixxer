<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_category`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `categories`
 */
class m161202_142550_create_user_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_category', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'categories_id' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_category-user_id',
            'user_category',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_category-user_id',
            'user_category',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `categories_id`
        $this->createIndex(
            'idx-user_category-categories_id',
            'user_category',
            'categories_id'
        );

        // add foreign key for table `categories`
        $this->addForeignKey(
            'fk-user_category-categories_id',
            'user_category',
            'categories_id',
            'categories',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_category-user_id',
            'user_category'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_category-user_id',
            'user_category'
        );

        // drops foreign key for table `categories`
        $this->dropForeignKey(
            'fk-user_category-categories_id',
            'user_category'
        );

        // drops index for column `categories_id`
        $this->dropIndex(
            'idx-user_category-categories_id',
            'user_category'
        );

        $this->dropTable('user_category');
    }
}
