<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auto_generations`.
 */
class m161202_143924_create_auto_generations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('auto_generations', [
            'id' => $this->primaryKey(),
            'generation_name' => $this->string(45),
            'model_id' => $this->string(45),
            'year_begin' => $this->string(45),
            'year_end' => $this->string(45),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auto_generations');
    }
}
