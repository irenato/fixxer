<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_info`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m161202_141935_create_user_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_info', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'image' => $this->string(45),
            'user_id' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_info-user_id',
            'user_info',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_info-user_id',
            'user_info',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_info-user_id',
            'user_info'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_info-user_id',
            'user_info'
        );

        $this->dropTable('user_info');
    }
}
