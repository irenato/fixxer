<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auto_brands`.
 */
class m161202_143358_create_auto_brands_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('auto_brands', [
            'id' => $this->primaryKey(),
            'brand_name' => $this->string(45),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auto_brands');
    }
}
