<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auto_modifications`.
 */
class m161202_144730_create_auto_modifications_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('auto_modifications', [
            'id' => $this->primaryKey(),
            'series_id' => $this->string(45),
            'model_id' => $this->string(45),
            'modification_name' => $this->string(45),
            'start_production_year' => $this->string(45),
            'end_production_year' => $this->string(45),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auto_modifications');
    }
}
