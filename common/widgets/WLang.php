<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 02.12.16
 * Time: 17:30
 */

namespace common\widgets;

use frontend\models\Lang;

class WLang extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        return $this->render('lang/view', [
            'current' => Lang::getCurrent(),
            'langs' => Lang::find()->where('id != :current_id', [':current_id' => Lang::getCurrent()->id])->all(),
        ]);
    }
}